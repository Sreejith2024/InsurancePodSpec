Pod::Spec.new do |spec|

  spec.name         = "UIComponents"
  spec.version      = "1.0.1"
  spec.summary      = "UIComponent library gives all shared resources"
  spec.description  = "UIComponent library gives all shared resources, images, colour, fonts, icons etc"
  spec.homepage     = "https://gitlab.com/Sreejith2024/UIComponents"
  spec.license      =  { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Sreejith2024" => "Sreejith2024@gmail.com" }
  spec.platform     = :ios, "11.0"
  spec.source       = { :git => "https://gitlab.com/Sreejith2024/UIComponents.git", :tag => "1.0.1" }
  spec.source_files = "UIComponents/**/*.swift"

end
