Pod::Spec.new do |spec|

  spec.name         = "UIComponents"
  spec.version      = "1.0.0"
  spec.summary      = "A short description of UIComponents.podspec"
  spec.description  = "Library for generic UI"
  spec.homepage     = "https://gitlab.com/Sreejith2024/UIComponents"
  spec.license      = "MIT"
  spec.author       = { "Sreejith2024" => "Sreejith2024@gmail.com" }
  spec.platform     = :ios, "11.0"
  spec.source       = { :git => "https://gitlab.com/Sreejith2024/UIComponents.git", :tag => "1.0.0" }
  spec.source_files = "UIComponents/**/*.swift"

end
